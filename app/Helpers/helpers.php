<?php

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

if (!function_exists('apiResponseFormat')) {
    function apiResponseFormat(
        bool $success = true,
        int $code = 200,
        string $message = 'Success',
        $result = null,
        \Illuminate\Support\MessageBag $errors = null
    ) {
        $response = compact('success', 'code', 'message');

        if ($result instanceof LengthAwarePaginator) {
            $response['result'] = $result->items();
            $response['meta']   = [
                'current_page'  => $result->currentPage(),
                'last_page'     => $result->lastPage(),
                'per_page'      => $result->perPage(),
                'total'         => $result->total(),
                'from'          => (($result->currentPage() - 1) * $result->perPage()) + 1,
            ];
            $response['meta']['to'] = ($response['meta']['from'] + $result->count()) - 1;
            $response['links'] = [
                'first' => $result->url(1),
                'prev'  => $result->previousPageUrl(),
                'next'  => $result->nextPageUrl(),
                'last'  => $result->url($result->lastPage()),
            ];
        } else {
            $response['result'] = $result;
        }

        if ($errors !== null) {
            $response['errors'] = $errors;
        }

        return $response;
    }
}

if (!function_exists('sql_like')) {
    function sql_like()
    {
        $db = config('database.default');

        if ($db === 'pgsql') {
            return 'ilike';
        }

        return 'like';
    }
}