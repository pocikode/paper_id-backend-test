<?php

namespace App\Rules;

use App\Models\Account;
use Illuminate\Contracts\Validation\Rule;

class AccountBelong implements Rule
{
    private $userId;

    private $selectMessage = 1;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $account = Account::find($value);

        if (!$account) {
            $this->selectMessage = 0;
            return false;
        }

        return $account->user_id == $this->userId;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $messages = [
            'The selected :attribute is not exists',
            'The selected :attribute is forbidden',
        ];

        return $messages[$this->selectMessage];
    }
}
