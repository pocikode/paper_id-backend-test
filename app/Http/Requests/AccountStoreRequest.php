<?php

namespace App\Http\Requests;

use App\Models\Account;

class AccountStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($account = $this->route('account')) {
            return $account->user_id === $this->user()->id;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'type' => 'string|max:50',
            'description' => 'string|max:255',
        ];
    }
}
