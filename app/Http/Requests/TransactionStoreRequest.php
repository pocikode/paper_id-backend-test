<?php

namespace App\Http\Requests;

use App\Models\Transaction;
use App\Rules\AccountBelong;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * @var Transaction $transaction
         */
        if ($transaction = $this->route('transaction')) {
            $transaction->load('account');

            return $transaction->account->user_id === $this->user()->id;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'account_id' => ['required', new AccountBelong($this->user()->id)],
            'amount' => 'required|numeric',
            'transaction_date' => 'required|date',
            'description' => 'string',
        ];
    }
}
