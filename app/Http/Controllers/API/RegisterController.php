<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\RegisterRequest;
use App\Models\User;

class RegisterController
{
    public function __invoke(RegisterRequest $request)
    {
        $data = $request->only('name', 'email');
        $data['password'] = bcrypt($request->password);

        $user = User::create($data);

        $token = auth()->login($user);

        return response()->api(array_merge(['user' => $user->toArray()], [
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expired_in'   => auth()->factory()->getTTL() * 60,
        ]), 201, 'User successfully registered');
    }
}