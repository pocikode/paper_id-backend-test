<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccountStoreRequest;
use App\Models\Account;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        if ($request->routeIs('accounts.index')) {
            $query = Account::query();
        } else {
            $query = Account::onlyTrashed();
        }

        $query = $query->where('user_id', auth()->id());

        if ($request->search) {
            $query = $query->where('name', sql_like(), "%{$request->search}%");
        }

        $accounts = $query->paginate($request->limit ?? 10);

        return response()->api($accounts);
    }

    public function show(Account $account)
    {
        if ($account->user_id !== auth()->id()) {
            throw new AuthorizationException('This action is unauthorized.');
        }

        return response()->api($account);
    }

    public function store(AccountStoreRequest $request)
    {
        $data = $request->only('name', 'type', 'description');
        $data['user_id'] = auth()->id();

        $account = Account::create($data);

        return response()->api($account, 201, 'Account successfully created');
    }

    public function update(AccountStoreRequest $request, Account $account)
    {
        $account->update(
            $request->only('name', 'type', 'description')
        );

        return response()->api(null, 200, 'Account successfully updated');
    }

    public function destroy(Account $account)
    {
        if ($account->user_id !== auth()->id()) {
            throw new AuthorizationException('This action is unauthorized.');
        }

        $account->delete();

        return response()->api(null, 200, 'Account successfully deleted');
    }

    public function restore($id)
    {
        $account = Account::onlyTrashed()->findOrFail($id);

        if ($account->user_id !== auth()->id()) {
            throw new AuthorizationException('This action is unauthorized.');
        }

        $account->restore();

        return response()->api(null, 200, 'Account successfully restored');
    }
}