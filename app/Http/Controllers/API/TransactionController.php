<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\TransactionStoreRequest;
use App\Models\Transaction;
use App\Services\TransactionSummary;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    private $dailyTransactionSummary;

    public function __construct(
        TransactionSummary $dailyTransactionSummary
    ) {
        $this->dailyTransactionSummary = $dailyTransactionSummary;
    }

    public function index(Request $request)
    {
        if ($request->routeIs('transactions.index')) {
            $query = Transaction::with('account');
        } else {
            $query = Transaction::onlyTrashed()->with('account');
        }

        $query = $query->whereHas('account', function(Builder $q) use ($request) {
            $q->where('user_id', '=', auth()->id());

            if ($request->account_name) {
                $q->where('name', sql_like(), "%{$request->account_name}%");
            }
        });

        if ($request->search) {
            $query = $query->where('name', sql_like(), "%{$request->search}");
        }

        if ($request->date) {
            $query = $query->where('transaction_date', $request->date);
        }

        $data = $query->paginate($request->limit ?? 10);

        return response()->api($data);
    }

    public function show(Transaction $transaction)
    {
        throw_if(
            $transaction->account->user_id !== auth()->id(),
            new AuthorizationException('This action is forbidden.')
        );

        return response()->api($transaction);
    }

    public function store(TransactionStoreRequest $request)
    {
        $transaction = Transaction::create(
            $request->only('name', 'account_id', 'amount', 'transaction_date', 'description')
        );

        return response()->api($transaction, 201, 'Transaction successfully created');
    }

    public function update(TransactionStoreRequest $request, Transaction $transaction)
    {
        $transaction->update(
            $request->only('name', 'account_id', 'amount', 'transaction_date', 'description')
        );

        return response()->api(null, 200, 'Transaction successfully updated');
    }

    public function destroy(Transaction $transaction)
    {
        throw_if(
            $transaction->account->user_id !== auth()->id(),
            new AuthorizationException('This action is forbidden.')
        );

        $transaction->delete();

        return response()->api(null, 200, 'Transaction successfully deleted');
    }

    public function restore($id)
    {
        $transaction = Transaction::onlyTrashed()
            ->with('account')
            ->findOrFail($id);

        throw_if(
            $transaction->account->user_id !== auth()->id(),
            new AuthorizationException('This action is forbidden.')
        );

        $transaction->restore();

        return response()->api(null, 200, 'Transaction successfully restored');
    }

    public function summary($base)
    {
        if ($base === 'daily') {
            $data = $this->dailyTransactionSummary->getDaily();
        } elseif ($base === 'monthly') {
            $data = $this->dailyTransactionSummary->getMonthly();
        } else {
            return response()->api(null, 400, 'summary base only supported daily or monthly', false);
        }

        return response()->api($data);
    }
}