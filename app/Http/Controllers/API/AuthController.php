<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = auth()->attempt($credentials)) {
            return response()->api(null, 401, 'Unauthorized', false);
        }

        return response()->api([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expired_in'   => auth()->factory()->getTTL() * 60,
        ]);
    }

    public function me()
    {
        return response()->api(
            auth()->user()
        );
    }

    public function logout()
    {
        auth()->logout();

        return response()->api(null, 200, 'Successfully logged out');
    }
}