<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function(
            $result = null,
            int $httpCode = 200,
            string $message = 'Success',
            bool $success = true,
            ?MessageBag $errors = null,
            array $headers = []
        ) {
            $response = Response::make(
                apiResponseFormat($success, $httpCode, $message, $result, $errors)
            );

            $response->setStatusCode($httpCode);

            if ($headers) {
                foreach ($headers as $key => $val) {
                    $response->header($key, $val);
                }
            }

            return $response;
        });
    }
}
