<?php

namespace App\Services;

use App\Models\Transaction;

class TransactionSummary
{
    /**
     * Get daily summary for last 14 days
     *
     * @param int $lastDays
     * @return \Illuminate\Support\Collection
     */
    public function getDaily(int $lastDays = 14)
    {
        $startDate = now()->subDays($lastDays - 1);
        $endDate   = now();

        $data = Transaction::query()
            ->select('transaction_date as date', \DB::raw("(SUM(amount)) as total"))
            ->whereHas('account', function ($q) {
                $q->where('user_id', auth()->id());
            })
            ->whereBetween('transaction_date', [
                $startDate->format('Y-m-d'),
                $endDate->format('Y-m-d')
            ])
            ->orderBy('date')
            ->groupBy('date')
            ->get();

        return $data;
    }

    public function getMonthly($lastMonth = 6)
    {
        $startDate = now()->subMonths($lastMonth - 1)->startOfMonth();
        $endDate   = now();

        $data = Transaction::query()
            ->select(\DB::raw("MONTH(`transaction_date`) as month"), \DB::raw("(SUM(amount)) as total"))
            ->whereHas('account', function ($q) {
                $q->where('user_id', auth()->id());
            })
            ->whereBetween('transaction_date', [
                $startDate->format('Y-m-d'),
                $endDate->format('Y-m-d')
            ])
            ->orderBy('transaction_date')
            ->groupBy('month')
            ->get();

        $data->each(function ($row) {
            $row['month'] = date('F', mktime(0, 0, 0, $row['month'], 10));
        });

        return $data;
    }
}