<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::create([
            'name' => 'Agus Supriyatna',
            'email' => 'agus@mail.com',
            'password' => bcrypt('123456'),
        ]);

        factory(User::class, 5)->create();
    }
}
