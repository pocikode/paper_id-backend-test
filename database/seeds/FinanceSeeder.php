<?php

use Illuminate\Database\Seeder;

class FinanceSeeder extends Seeder
{
    /** @var \Faker\Generator */
    private $faker;

    /** @var string[][] */
    private $accountType;

    public function __construct()
    {
        $this->faker = app(\Faker\Generator::class);

        $this->accountType = [
            ['name' => 'Bank BCA', 'type' => 'bank'],
            ['name' => 'Bank Mandiri', 'type' => 'bank'],
            ['name' => 'Bank BNI', 'type' => 'bank'],
            ['name' => 'OVO', 'type' => 'eWallet'],
            ['name' => 'Gopay', 'type' => 'eWallet'],
            ['name' => 'Dana', 'type' => 'eWallet'],
            ['name' => 'Cash', 'type' => 'cash'],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();

        /** @var \App\Models\User $user */
        foreach ($users as $user) {
            $this->createUserAccounts($user);

            $accounts = $user->accounts()->get();

            foreach ($accounts as $account) {
                $this->createTransaction($account);
            }
        }

        $this->seedAgusTransaction(500);
    }

    protected function seedAgusTransaction($amount = 200)
    {
        /** @var \App\Models\User $user */
        $user = \App\Models\User::where('email', 'agus@mail.com')->first();

        $accounts = $user->accounts()->get();

        /** @var \App\Models\Account $account */
        foreach ($accounts as $account) {
            $transaction = [];

            $startDate = now()->startOfYear();
            $endDate   = now();

            for ($i = 0; $i < $amount; $i++) {
                $transaction[] = [
                    'account_id' => $account->id,
                    'name' => $this->faker->city,
                    'amount' => $this->faker->numberBetween(50000, 5000000),
                    'transaction_date' => date('Y-m-d', mt_rand(
                        $startDate->timestamp,
                        $endDate->timestamp,
                    )),
                    'description' => $this->faker->paragraph(1),
                ];
            }

            $account->transactions()->insert($transaction);
        }
    }

    protected function createUserAccounts(\App\Models\User $user, $amount = 5)
    {
        $accounts = [];

        for ($i = 0; $i < $amount; $i++) {
            $account = $this->accountType[array_rand($this->accountType)];

            $accounts[] = [
                'user_id' => $user->id,
                'name' => $account['name'],
                'type' => $account['type'],
                'description' => $this->faker->paragraph(1),
            ];
        }

        $user->accounts()->insert($accounts);
    }

    protected function createTransaction(\App\Models\Account $account)
    {
        $transactions = [];

        $amount = random_int(5, 20);

        for ($i = 0; $i < $amount; $i++) {
            $transactions[] = [
                'account_id' => $account->id,
                'name' => $this->faker->city,
                'amount' => $this->faker->numberBetween(50000, 5000000),
                'transaction_date' => $this->faker->date(),
                'description' => $this->faker->paragraph(1),
            ];
        }

        $account->transactions()->insert($transactions);
    }
}
