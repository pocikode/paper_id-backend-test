<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\RegisterController');

Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('me', 'API\AuthController@me');
    Route::post('logout', 'API\AuthController@logout');

    Route::get('accounts/trash', 'API\AccountController@index');
    Route::post('accounts/trash/{id}/restore', 'API\AccountController@restore');
    Route::apiResource('accounts', 'API\AccountController');

    Route::get('transactions/trash', 'API\TransactionController@index');
    Route::post('transactions/trash/{id}/restore', 'API\TransactionController@restore');
    Route::get('transactions/summary/{base}', 'API\TransactionController@summary');
    Route::apiResource('transactions', 'API\TransactionController');
});
